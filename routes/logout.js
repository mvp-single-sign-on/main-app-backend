const express = require('express');
const router = express.Router();
const config = require('./../config');
const { patchFetch, getFetch } = require('../utils/fetch')
const { logoutFusion } = require('../service/fusion-hook')

router.get('/', async (req, res) => {
  console.log('====main app ====> logout user remove session')
  console.log(req.session.token, '==token ==>before destroy')

  const userInfo = await getFetch(`https://fusionauth.ilotusland.asia/oauth2/userinfo`, {}, req.session.token)
  const userId = userInfo.sub

  console.log(userInfo, '==userInfo====> logout main app')
  await logoutFusion(userId)
  // delete the session
  req.session.destroy();
  console.log(req.session, '==token ==>after destroy')



  // patchFetch(`https://fusionauth.ilotusland.asia/api/user/23e218bb-8996-4bcb-b68c-56e649fe49c3`)


  // end FusionAuth session
  res.redirect(`https://fusionauth.ilotusland.asia/oauth2/logout?client_id=${config.clientID}`);
});

// const logoutFusion = (userId) => {
//   return patchFetch(`https://fusionauth.ilotusland.asia/api/user/${userId}`,
//     {
//       user: {
//         data: {
//           isLoggedIn: false
//         }
//       }
//     },
//     {},
//     {
//       Authorization: config.apiKey
//     }
//   )
// }

module.exports = router;
