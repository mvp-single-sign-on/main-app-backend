const { patchFetch, getFetch } = require('../utils/fetch')
const config = require('./../config');

const logoutFusion = (userId) => {
  return patchFetch(`https://fusionauth.ilotusland.asia/api/user/${userId}`,
    {
      user: {
        data: {
          isLoggedIn: false
        }
      }
    },
    {},
    {
      Authorization: config.apiKey
    }
  )
}

module.exports = { logoutFusion }